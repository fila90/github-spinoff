const URL = 'https://api.github.com';
const HEADERS = new Headers({
	'Accept': 'application/vnd.github.v3+json',
})

/**
* @name fetchUsers
* @desc get a full list of users
* @param {Number} since
*/
const fetchUsers = (since) => {
	return fetch(`${URL}/users?since=${since}`, {
		headers: HEADERS
	})
	.then(handleErrors)
	.then(response => response.json())
}

/**
* @name fetchUser
* @desc get details for single user
* @param {string} userName
*/
const fetchUser = (userName) => {
	if(!userName) throw new Error(`User id is required!`);

	return fetch(`${URL}/users/${userName}`, {
		headers: HEADERS
	})
	.then(handleErrors)
	.then(response => response.json());
}

export {
	fetchUsers,
	fetchUser,
}

/**
* @name handleErrors
* @desc handle errors midleware, if response not ok throw error
*/
const handleErrors = (req) => {
	if(req.ok) return req

	throw new Error (req.status);
}
