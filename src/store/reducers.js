import { handleActions } from 'redux-actions'
import initialState from './state.js'

export const reducer = handleActions({
	ADD_USERS: (state, action) => {
		return {
			...state,
			users: [
				...state.users,
				...action.payload
			],
		}
	},
	SET_ACTIVE_USER: (state, action) => {
		return {
			...state,
			activeUser: action.payload,
		}
	},
	SET_SINCE: (state, action) => {
		return {
			...state,
			since: action.payload,
		}
	},
	TOGGLE_LOADER: (state, action) => {
		return {
			...state,
			loader: action.payload,
		}
	},
	TOGGLE_BTN_LOADER: (state, action) => {
		return {
			...state,
			btnLoader: action.payload,
		}
	},
	TOGGLE_ERROR: (state, action) => {
		return {
			...state,
			showError: action.payload,
		}
	}
}, initialState)
