// used mostly for reference, to get clear picture how my initial state should look like
export default {
	users: [],
	activeUser: {},
	since: 0, // used to load more users
	loader: true, // set to true to skip initial rerender when you toggle loader
	btnLoader: false,
	showError: false,
};
