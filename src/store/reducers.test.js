import {
	ADD_USERS,
	SET_ACTIVE_USER,
	SET_SINCE,
	TOGGLE_LOADER,
	TOGGLE_BTN_LOADER,
	TOGGLE_ERROR,
} from './actions.js'
import initialState from './state'
import { reducer } from './reducers'

const users = [{
		"login": "mojombo",
		"id": 1,
		"avatar_url": "https://avatars0.githubusercontent.com/u/1?v=4",
		"html_url": "https://github.com/mojombo",
	},
	{
		"login": "defunkt",
		"id": 2,
		"avatar_url": "https://avatars0.githubusercontent.com/u/2?v=4",
		"html_url": "https://github.com/defunkt",
	},
	{
		"login": "pjhyett",
		"id": 3,
		"avatar_url": "https://avatars0.githubusercontent.com/u/3?v=4",
		"html_url": "https://github.com/pjhyett",
	},
	{
		"login": "wycats",
		"id": 4,
		"avatar_url": "https://avatars0.githubusercontent.com/u/4?v=4",
		"html_url": "https://github.com/wycats",
	}
];

describe('reducer test', () => {
	it('should return initial state', () => {
		expect(reducer(undefined, {})).toEqual(initialState)
	})

	it('should add new users', () => {
		expect(reducer(initialState, {
			type: ADD_USERS,
			payload: users,
		})).toEqual({
			users,
			activeUser: {},
			since: 0,
			loader: true,
			btnLoader: false,
			showError: false,
		})
	})

	it('should set active user', () => {
		expect(reducer(initialState, {
			type: SET_ACTIVE_USER,
			payload: users[0],
		})).toEqual({
			users: [],
			activeUser: users[0],
			since: 0,
			loader: true,
			btnLoader: false,
			showError: false,
		})
	})

	it('should set since', () => {
		expect(reducer(initialState, {
			type: SET_SINCE,
			payload: 68,
		})).toEqual({
			users: [],
			activeUser: {},
			since: 68,
			loader: true,
			btnLoader: false,
			showError: false,
		})
	})

	it('should toggle main loader', () => {
		expect(reducer(initialState, {
			type: TOGGLE_LOADER,
			payload: false,
		})).toEqual({
			users: [],
			activeUser: {},
			since: 0,
			loader: false,
			btnLoader: false,
			showError: false,
		})
	})

	it('should toggle btn loader', () => {
		expect(reducer(initialState, {
			type: TOGGLE_BTN_LOADER,
			payload: true,
		})).toEqual({
			users: [],
			activeUser: {},
			since: 0,
			loader: true,
			btnLoader: true,
			showError: false,
		})
	})

	it('should toggle error', () => {
		expect(reducer(initialState, {
			type: TOGGLE_ERROR,
			payload: true,
		})).toEqual({
			users: [],
			activeUser: {},
			since: 0,
			loader: true,
			btnLoader: false,
			showError: true,
		})
	})
})
