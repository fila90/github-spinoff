import * as API from '../helpers/api.js'
import { createActions } from 'redux-actions'

export const ADD_USERS = 'ADD_USERS'
export const SET_ACTIVE_USER = 'SET_ACTIVE_USER'
export const SET_SINCE = 'SET_SINCE'
export const TOGGLE_LOADER = 'TOGGLE_LOADER'
export const TOGGLE_BTN_LOADER = 'TOGGLE_BTN_LOADER'
export const TOGGLE_ERROR = 'TOGGLE_ERROR'

export const {
	addUsers,
	setActiveUser,
	setSince,
	toggleLoader,
	toggleBtnLoader,
	toggleError,
} = createActions({},
	ADD_USERS,
	SET_ACTIVE_USER,
	SET_SINCE,
	TOGGLE_LOADER,
	TOGGLE_BTN_LOADER,
	TOGGLE_ERROR
)

/**
 * @name fetchUsers
 * @desc show desired loaders, fetch users, store new data
 * @param {Number} since
 * @param {Bool} loader
 * @param {Bool} btnLoader
 */
export const fetchUsers = ({ since = 0, loader }) => {
	return dispatch => {
		dispatch(toggleLoader(loader || false))
		dispatch(toggleBtnLoader(true))
		dispatch(toggleError(false))
		return API.fetchUsers(since)
			.then(response => {
				const users = response
				const since = users[users.length - 1].id
				dispatch(addUsers(users))
				dispatch(setSince(since))
				dispatch(toggleBtnLoader(false))
				dispatch(toggleLoader(false))
			})
			.catch(err => {
				dispatch(toggleError(true))
				dispatch(toggleLoader(false))
				dispatch(toggleBtnLoader(false))
			})
	}
}

/**
 * @name fetchUser
 * @desc fetch single user, called if activeUsers is not found,
 * 			 usualy after refresh in /user/:userName route
 * @param {string} userName
 */
export const fetchUser = (userName) => {
	return dispatch => {
		dispatch(toggleLoader(true))
		dispatch(toggleBtnLoader(true))
		dispatch(toggleError(false))
		return API.fetchUser(userName)
			.then(response => {
				dispatch(setActiveUser(response))
				dispatch(toggleLoader(false))
			})
			.catch(err => {
				dispatch(toggleError(true))
				dispatch(toggleLoader(false))
				dispatch(toggleBtnLoader(false))
			})
	}
}
