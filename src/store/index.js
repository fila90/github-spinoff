import {
	createStore,
	applyMiddleware
} from 'redux'
import thunkMiddleware from 'redux-thunk'
import {
	reducer
} from './reducers.js'
import initialState from './state.js'

const store = createStore(
	reducer,
	initialState,
	applyMiddleware(thunkMiddleware)
)

export default store
