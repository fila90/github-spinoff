import React from 'react'
import PropTypes from 'prop-types'

const Navigation = ({ onBtnClick }) => (
	<nav className="navigation">
		<button type="button" className="btn" onClick={() => onBtnClick()}>Go Back</button>
	</nav>
)

Navigation.propTypes = {
	onBtnClick: PropTypes.func.isRequired,
}

export default Navigation
