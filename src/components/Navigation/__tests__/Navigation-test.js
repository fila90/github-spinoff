import React from 'react'
import renderer from 'react-test-renderer'
import Navigation from '../'

it('should render correctly', () => {
	const onBtnClick = jest.fn();
	const tree = renderer
		.create(<Navigation onBtnClick={onBtnClick} />)
		.toJSON();
	expect(tree).toMatchSnapshot();
})
