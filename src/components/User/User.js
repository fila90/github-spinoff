import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

const User = ({ user, onUserClick }) => (
	<div className="col-sm-12 user">
		<img src={user.avatar_url}
				 alt={user.login} />
		<h4>{user.login}</h4>

		<NavLink to={`/users/${user.login}`}
					onClick={() => onUserClick(user)}>
			Details
		</NavLink>
	</div>
)

User.propTypes = {
	user: PropTypes.shape({
		avatar_url: PropTypes.string.isRequired,
		login: PropTypes.string.isRequired,
		id: PropTypes.number.isRequired,
	}).isRequired,
	onUserClick: PropTypes.func.isRequired,
}

export default User
