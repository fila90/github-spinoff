import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import { MemoryRouter as Router, withRouter } from 'react-router-dom'
import User from '../'

const user = {
	"login": "mojombo",
	"id": 1,
	"avatar_url": "https://avatars0.githubusercontent.com/u/1?v=4",
	"html_url": "https://github.com/mojombo",
}
const onUserClick = jest.fn();

it('should render correctly', () => {
	const tree = renderer
		.create(<Router><User user={user} onUserClick={onUserClick} /></Router>)
		.toJSON()
	expect(tree).toMatchSnapshot()
})

it('should render user with name mojombo', () => {
	const component = mount(<Router><User user={user} onUserClick={onUserClick} /></Router>)
	expect(component.find('h4').text()).toBe(user.login)
})
