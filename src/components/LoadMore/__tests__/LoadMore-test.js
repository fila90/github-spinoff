import React from 'react'
import renderer from 'react-test-renderer'
import {shallow} from 'enzyme';
import LoadMore from '../'

it('renders error correctly', () => {
	const btnLoader = true;
	const since = 0;
	const onBtnClick = jest.fn();
	const tree = renderer
		.create(<LoadMore btnLoader={btnLoader} since={since} onBtnClick={onBtnClick} />)
		.toJSON();
	expect(tree).toMatchSnapshot();
})

it('renders btn correctly', () => {
	const btnLoader = false;
	const since = 0;
	const onBtnClick = jest.fn();
	const tree = renderer
		.create(<LoadMore btnLoader={btnLoader} since={since} onBtnClick={onBtnClick} />)
		.toJSON();
	expect(tree).toMatchSnapshot();
})

it('should renderer error message', () => {
	const btnLoader = true;
	const since = 0;
	const onBtnClick = jest.fn();
	const tree = shallow(<LoadMore btnLoader={btnLoader} since={since} onBtnClick={onBtnClick} />)
	expect(tree.text()).toEqual('Loading more users...');
})

it('should renderer button message', () => {
	const btnLoader = false;
	const since = 0;
	const onBtnClick = jest.fn();
	const tree = shallow(<LoadMore btnLoader={btnLoader} since={since} onBtnClick={onBtnClick} />)
	expect(tree.text()).toEqual('Load more users');
})
