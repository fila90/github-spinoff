import React from 'react'
import PropTypes from 'prop-types'

const LoadMore = ({ btnLoader, since, onBtnClick }) => {
	const content = btnLoader
		? <h3>Loading more users...</h3>
		: <button type="button"
							className="btn load-more"
							onClick={() => onBtnClick({since, btnLoader: true})}>Load more users</button>

	return (
		<footer className="list-footer">
			{content}
		</footer>
	)
}

LoadMore.propTypes = {
	btnLoader: PropTypes.bool.isRequired,
	since: PropTypes.number.isRequired,
	onBtnClick: PropTypes.func.isRequired,
}

export default LoadMore
