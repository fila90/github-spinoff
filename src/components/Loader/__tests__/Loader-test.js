import React from 'react'
import renderer from 'react-test-renderer'
import Loader from '../'

it('should renderer correctly', () => {
	const tree = renderer
		.create(<Loader />)
		.toJSON();
	expect(tree).toMatchSnapshot();
})
