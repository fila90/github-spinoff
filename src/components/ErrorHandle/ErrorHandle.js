import React from 'react'

const ErrorHandle = () => <div>Ooops. Error fetching data. Please try again later.</div>

export default ErrorHandle
