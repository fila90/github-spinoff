import React from 'react'
import renderer from 'react-test-renderer'
import ErrorHandle from '../'

it('renders correctly', () => {
	const tree = renderer
		.create(<ErrorHandle />)
		.toJSON();
		expect(tree).toMatchSnapshot();
})
