import React from 'react'
import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme'
import UserDetails from '../'

const activeUser = {
	"login": "mojombo",
	"id": 1,
	"node_id": "MDQ6VXNlcjE=",
	"avatar_url": "https://avatars0.githubusercontent.com/u/1?v=4",
	"gravatar_id": "",
	"url": "https://api.github.com/users/mojombo",
	"html_url": "https://github.com/mojombo",
	"followers_url": "https://api.github.com/users/mojombo/followers",
	"following_url": "https://api.github.com/users/mojombo/following{/other_user}",
	"gists_url": "https://api.github.com/users/mojombo/gists{/gist_id}",
	"starred_url": "https://api.github.com/users/mojombo/starred{/owner}{/repo}",
	"subscriptions_url": "https://api.github.com/users/mojombo/subscriptions",
	"organizations_url": "https://api.github.com/users/mojombo/orgs",
	"repos_url": "https://api.github.com/users/mojombo/repos",
	"events_url": "https://api.github.com/users/mojombo/events{/privacy}",
	"received_events_url": "https://api.github.com/users/mojombo/received_events",
	"type": "User",
	"site_admin": false
}
it('should render correctly', () => {
	const tree = renderer
		.create(
			<UserDetails activeUser={activeUser} />
		)
		.toJSON();
	expect(tree).toMatchSnapshot();
})

it('should display name and ID correctly', () => {
	const component = mount(<UserDetails activeUser={activeUser} />)
	expect(component.find('h2').text()).toBe(`Name: ${activeUser.login}`)
	expect(component.find('h4').text()).toBe(`#id: ${activeUser.id}`)
})
