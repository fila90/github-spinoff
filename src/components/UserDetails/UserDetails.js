import React from 'react'
import PropTypes from 'prop-types'

const UserDetails = ({ activeUser }) => {
	return (
		<div className="row user-details">
			<div className="col-sm-12">
				<h2>Name: {activeUser.login}</h2>
				<h4>#id: {activeUser.id}</h4>
			</div>
			<div className="col-sm-12">
				<img className="fit-img" src={activeUser.avatar_url} alt={activeUser.login} />
			</div>
			<div className="col-sm-12">
				<a href={activeUser.html_url} target="_blank">Go To Profile</a>
			</div>
		</div>
	)
}

UserDetails.propTypes = {
	activeUser: PropTypes.shape({
		avatar_url: PropTypes.string.isRequired,
		html_url: PropTypes.string.isRequired,
		login: PropTypes.string.isRequired,
		id: PropTypes.number.isRequired,
	}).isRequired
}

export default UserDetails
