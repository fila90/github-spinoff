import React from 'react'
import renderer from 'react-test-renderer'
import { MemoryRouter as Router, withRouter } from 'react-router-dom'
import { mount, shallow } from 'enzyme'
import UsersList from '../'

const users = [{
		"login": "mojombo",
		"id": 1,
		"avatar_url": "https://avatars0.githubusercontent.com/u/1?v=4",
		"html_url": "https://github.com/mojombo",
	},
	{
		"login": "defunkt",
		"id": 2,
		"avatar_url": "https://avatars0.githubusercontent.com/u/2?v=4",
		"html_url": "https://github.com/defunkt",
	},
	{
		"login": "pjhyett",
		"id": 3,
		"avatar_url": "https://avatars0.githubusercontent.com/u/3?v=4",
		"html_url": "https://github.com/pjhyett",
	},
	{
		"login": "wycats",
		"id": 4,
		"avatar_url": "https://avatars0.githubusercontent.com/u/4?v=4",
		"html_url": "https://github.com/wycats",
	}
];
const onUserClick = jest.fn();

it('should renderer correctly', () => {
	const tree = renderer
		.create(<Router><UsersList users={users} onUserClick={onUserClick} /></Router>)
		.toJSON();
	expect(tree).toMatchSnapshot();
});

it('should render 4 users', () => {
	const component = mount(<Router><UsersList users={users} onUserClick={onUserClick} /></Router>);
	const childNodes = component.find('.users-list-wrap').children();
	expect(childNodes.length).toBe(4);
})

it('last child shoul have name wycats', () => {
	const component = mount(<Router><UsersList users={users} onUserClick={onUserClick} /></Router>);
	const lastNode = component.find('.user').last();
	expect(lastNode.find('h4').text()).toBe('wycats')
})
