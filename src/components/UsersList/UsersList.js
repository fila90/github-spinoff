import React from 'react'
import PropTypes from 'prop-types'
import User from '../User'

const UsersList = ({ users, onUserClick }) => (
	<div className="row users-list-wrap">
		{users.map(user => <User key={user.id} user={user} onUserClick={onUserClick} />)}
	</div>
)

UsersList.propTypes = {
	users: PropTypes.array.isRequired,
	onUserClick: PropTypes.func.isRequired,
}

export default UsersList
