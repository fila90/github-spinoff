import React from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

import Home from './views/Home.js'
import User from './views/User.js'

const BaseRouting = () => (
		<Router>
			<main className="container-fluid">
				<Route exact path='/' component={Home} />
				<Route path='/users/:userName' component={User} />
			</main>
		</Router>
)

export default BaseRouting
