import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'
import store from './store'

import BaseRouting from './router';
import './assets/style/index.css';

ReactDOM.render(
	<Provider store={store}>
		<BaseRouting />
	</Provider>,
	document.getElementById('root'));
registerServiceWorker();
