import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchUser } from '../store/actions.js'

import Loader from '../components/Loader'
import ErrorHandle from '../components/ErrorHandle'
import Navigation from '../components/Navigation'
import UserDetails from '../components/UserDetails'

class User extends React.Component {
	// componentWillMount is being depricated so we roll with this now
	componentDidMount() {
		// if activeUser is not stored, fetch from API
		if (!this.props.activeUser.id) {
			const { userName } = this.props.match.params;
			this.props.fetchUser(userName)
		}
	}

	goBack() {
		this.props.history.goBack();
	}

	render() {
		const { activeUser, loader, showError } = this.props;

		if(loader) return <Loader />

		return (
			<div className="container">
				<Navigation onBtnClick={this.goBack.bind(this)} />
				<h1>User Details</h1>
				{showError
					? <ErrorHandle />
					: <UserDetails activeUser={activeUser} />}
			</div>
		)
	}
}

User.propTypes = {
	activeUser: PropTypes.object.isRequired,
	loader: PropTypes.bool.isRequired,
	showError: PropTypes.bool.isRequired,
}

const mapStateToProps = state => {
	return {
		activeUser: state.activeUser,
		loader: state.loader,
		showError: state.showError,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		fetchUser: userName => {
			dispatch(fetchUser(userName));
		}
	}
}

const UserContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(User)

export default UserContainer
