import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
	setActiveUser,
	fetchUsers,
} from '../store/actions'

import Loader from '../components/Loader'
import ErrorHandle from '../components/ErrorHandle'
import UsersList from '../components/UsersList'
import LoadMore from '../components/LoadMore'

class Home extends React.Component {
	// componentWillMount is being depricated so we roll with this now
	componentDidMount() {
		// if first render, fetch data
		if (!this.props.users.length && this.props.since === 0) {
			this.props.fetchUsers({
				since: 0,
				loader: true
			});
		}
	}

	render() {
		const { users, since, loader, btnLoader, showError, fetchUsers, onUserClick } = this.props;

		if(loader) return <Loader />

		return (
			<div className="container">
				<h1>GitHub Spinoff</h1>
				{showError
					? <ErrorHandle />
					: <UsersList users={users} onUserClick={onUserClick} />}
				<LoadMore btnLoader={btnLoader} since={since} onBtnClick={fetchUsers} />
			</div>
		)
	}
}

Home.propTypes = {
	users: PropTypes.array.isRequired,
	since: PropTypes.number.isRequired,
	loader: PropTypes.bool.isRequired,
	btnLoader: PropTypes.bool.isRequired,
	onUserClick: PropTypes.func.isRequired,
	fetchUsers: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
	return {
		users: state.users,
		since: state.since,
		loader: state.loader,
		btnLoader: state.btnLoader,
		showError: state.showError,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onUserClick: activeUser => {
			dispatch(setActiveUser(activeUser))
		},
		fetchUsers: props => {
			dispatch(fetchUsers(props))
		},
	}
}

const HomeContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Home)

export default HomeContainer
